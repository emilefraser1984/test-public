## Syntax

```syntax
- Italics (* or _)
- Bold/Strong (** or __)
- Both Bold & Italics (**_)
- Strikethrough (~~) with {pymdownx.tilde active}, into <del></del> tags
- Underscript (~) with {pymdownx.tilde active}, into <sub></sub> tags
- Highlight (==) with {pymdownx.mark active} into <mark></mark> tags
- Underlined (^^)
```

## Key Points

- Markdown treats asterisks (`*`) and underscores (`_`) as indicators of emphasis. 
- Text wrapped with one `*` or `_` will be wrapped with an HTML `<em>` tag; 
- Double `*`'s or `_`'s will be wrapped with an HTML `<strong>` tag. E.g., this input
- Emphasis can be used in the middle of a word
- To produce a literal asterisk or underscore at a position where it would otherwise 
    be used as an emphasis delimiter, you can backslas escape it

## Use Cases

### Standard Usage

```language-markdown
Italics, This will show italic emphasis: *asterisks* or _underscores_.  
Bold, This will show bold emphasis:  **asterisks** or __underscores__.  
Bold & Italics,This show a combination  **asterisks and _underscores_**.  
Strikethrough, Strikethrouh show like this:  ~~Scratch this~~.  
Subscript, Show like this:  H~2SO~4
Highlight, Highlighing can be done like this: ==this text==  
Caret ^^Underlined Text^^  
```

```language-html
<em>single asterisks</em>
<em>single underscores</em>
<strong>double asterisks</strong>
<strong>double underscores</strong>
<p>Some of these words <em>are emphasized</em>.
Some of these words <em>are emphasized also</em>.</p>
<p>Use two asterisks for <strong>strong emphasis</strong>.
Or, if you prefer, <strong>use two underscores instead</strong>.</p>
```

**Output**  
Italics, This will show italic emphasis: *asterisks* or _underscores_.  
Bold, This will show bold emphasis: **asterisks** or __underscores__.  
Bold & Italics,This show a combination **asterisks and _underscores_**.  
Strikethrouh show like this: ~~Scratch this~~.  
Subscript, Show like this: CH~3~CH~2~OH  
Highlight, Highlighing can be done like this: ==this text==.  
Highlight, Highlighing can be done like this: this==text.  
Caret, Will yield underlined text like this: ^^Underlined Text^^  

### Literal Escape

```language-markdown
\*this text is surrounded by literal asterisks\*
```

```language-html

```

**Ouput**  
\*this text is surrounded by literal asterisks\*


Here is some {--*incorrect*--} Markdown.  I am adding this{++ here.++}.  Here is some more {--text
 that I am removing--}text.  And here is even more {++text that I 
 am ++}adding.{~~

~>  ~~}Paragraph was deleted and replaced with some spaces.{~~  ~>

~~}Spaces were removed and a paragraph was added.

And here is a comment on {==some
 ==text== ==}{>>This works quite well. I just wanted to comment on it.<<}. Substitutions {~~is~>are~~} great!

General block handling.

{--

* test
* test
* test
    * test
* test

--}

{++

* test
* test
* test
    * test
* test

++}
